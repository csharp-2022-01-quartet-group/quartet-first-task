﻿
namespace Hogwarts.DAL.Entities
{
    /// <summary>
    /// Категория
    /// </summary>
    public class Category : EntityBase
    {
        /// <summary>
        /// Наименование категории
        /// </summary>
        public string Name { get; set; }

        //public override string ToString() => $"{CategoryId}";
    }

}
