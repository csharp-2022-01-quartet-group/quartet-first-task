﻿using AutoMapper;
using Hogwarts.DAL.DTO;
using Hogwarts.DAL.Entities;

namespace Hogwarts.BLL.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<OrderDTO, Order>().ReverseMap();
            CreateMap<ScheduleDTO, Schedule>().ReverseMap();
        }
    }
}
