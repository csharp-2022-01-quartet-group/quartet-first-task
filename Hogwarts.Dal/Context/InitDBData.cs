﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hogwarts.DAL.Entities;

namespace Hogwarts.DAL
{
    public static class InitDBData
    {
        public static void Start(AppDbContext db)
        {
            if (!db.Roles.Any())
            {
                db.Roles.Add(new Role { Id = 1, Name="Administrator" });
                db.Roles.Add(new Role { Id = 2, Name = "Moderator" });
                db.Roles.Add(new Role { Id = 3, Name = "Teacher" });
                db.Roles.Add(new Role { Id = 4, Name = "Student" });
            }
            if (!db.Contacts.Any())
            {
                db.Contacts.Add(new Contact { Id = 1, Telephone = "89993690001", Email = "Director@hogwarts.com" });
                db.Contacts.Add(new Contact { Id = 2, Telephone = "89993690002", Email = "Decan.Griffindor@hogwarts.com" });
                db.Contacts.Add(new Contact { Id = 3, Telephone = "89993690003", Email = "Decan.Slizerin@hogwarts.com" });
                db.Contacts.Add(new Contact { Id = 4, Telephone = "89993690004", Email = "Decan.Puffenduy@hogwarts.com" });
                db.Contacts.Add(new Contact { Id = 5, Telephone = "89993690005", Email = "Decan.Kogtevran@hogwarts.com" });
                db.Contacts.Add(new Contact { Id = 6, Telephone = "89993690006", Email = "R.Hagrid@hogwarts.com" });
                db.Contacts.Add(new Contact { Id = 7, Telephone = "89993690007", Email = "R.Lupin@hogwarts.com" });
                db.Contacts.Add(new Contact { Id = 8, Telephone = "89993690008", Email = "D.Ambridj@hogwarts.com" });
                db.Contacts.Add(new Contact { Id = 9, Telephone = "89993690009", Email = "A.Gryum@hogwarts.com" });
                db.Contacts.Add(new Contact { Id = 10, Telephone = "89993690010", Email = "S.Trelony@hogwarts.com" });
                db.Contacts.Add(new Contact { Id = 11, Telephone = "89993690011", Email = "Z.Lokons@hogwarts.com" });
                db.Contacts.Add(new Contact { Id = 12, Telephone = "89993690012", Email = "G.Potter@hogwarts.com" });
                db.Contacts.Add(new Contact { Id = 13, Telephone = "89993690013", Email = "R.Uizly@hogwarts.com" });
                db.Contacts.Add(new Contact { Id = 14, Telephone = "89993690014", Email = "H.Granger@hogwarts.com" });
                db.Contacts.Add(new Contact { Id = 15, Telephone = "89993690015", Email = "J.Uizly@hogwarts.com" });
                db.Contacts.Add(new Contact { Id = 16, Telephone = "89993690016", Email = "F.Uizly@hogwarts.com" });
                db.Contacts.Add(new Contact { Id = 17, Telephone = "89993690017", Email = "D.Uizly@hogwarts.com" });
                db.Contacts.Add(new Contact { Id = 18, Telephone = "89993690018", Email = "S.Finnigan@hogwarts.com" });
                db.Contacts.Add(new Contact { Id = 19, Telephone = "89993690019", Email = "N.Dolgopups@hogwarts.com" });
                db.Contacts.Add(new Contact { Id = 20, Telephone = "89993690020", Email = "P.Lavgud@hogwarts.com" });
            }
            if (!db.Users.Any())
            {
                db.Users.Add(new User { Id = 1, Name = "Альбус", Surname = "Дамблдор", Sex = "М", ContactId = 1, Login = "Albus", Password = "Albus", RoleId = 1, Birthday = "26.07.1881" });
                db.Users.Add(new User { Id = 2, Name = "Минерва", Surname = "Макгонагалл", Sex = "Ж", ContactId = 2, Login = "Minerva", Password = "Minerva", RoleId = 2, Birthday = "04.10.1935" });
                db.Users.Add(new User { Id = 3, Name = "Северус", Surname = "Снегг", Sex = "М", ContactId = 3, Login = "Severus", Password = "Severus", RoleId = 2, Birthday = "09.05.1960" });
                db.Users.Add(new User { Id = 4, Name = "Помона", Surname = "Стебль", Sex = "Ж", ContactId = 4, Login = "Pomona", Password = "Pomona", RoleId = 2, Birthday = "15.05.1932" });
                db.Users.Add(new User { Id = 5, Name = "Филиус", Surname = "Флитвик", Sex = "М", ContactId = 5, Login = "Filius", Password = "Filius", RoleId = 2, Birthday = "17.10.1920" });
                db.Users.Add(new User { Id = 6, Name = "Рубеус", Surname = "Хагрид", Sex = "М", ContactId = 6, Login = "Rubeus", Password = "Rubeus", RoleId = 3, Birthday = "06.12.1928" });
                db.Users.Add(new User { Id = 7, Name = "Римус", Surname = "Люпин", Sex = "М", ContactId = 7, Login = "Rimus", Password = "Rimus", RoleId = 3, Birthday = "10.03.1960" });
                db.Users.Add(new User { Id = 8, Name = "Долорес", Surname = "Амбридж", Sex = "Ж", ContactId = 8, Login = "Dolores", Password = "Dolores", RoleId = 3, Birthday = "26.08.1959" });
                db.Users.Add(new User { Id = 9, Name = "Аластор", Surname = "Грюм", Sex = "М", ContactId = 9, Login = "Alastor", Password = "Alastor", RoleId = 3, Birthday = "27.07.1948" });
                db.Users.Add(new User { Id = 10, Name = "Сивилла", Surname = "Трелони", Sex = "Ж", ContactId = 10, Login = "Sivilla", Password = "Sivilla", RoleId = 3, Birthday = "09.03.1955" });
                db.Users.Add(new User { Id = 11, Name = "Златопуст", Surname = "Локонс", Sex = "М", ContactId = 11, Login = "Zlatopust", Password = "Zlatopust", RoleId = 3, Birthday = "26.01.1964" });
                db.Users.Add(new User { Id = 12, Name = "Гарри", Surname = "Поттер", Sex = "М", ContactId = 12, Login = "PotterG", Password = "PotterG", RoleId = 4, Birthday = "31.07.1980" });
                db.Users.Add(new User { Id = 13, Name = "Рональд", Surname = "Уизли", Sex = "М", ContactId = 13, Login = "UizlyR", Password = "UizlyR", RoleId = 4, Birthday = "01.03.1980" });
                db.Users.Add(new User { Id = 14, Name = "Гермиона", Surname = "Грейнджер", Sex = "Ж", ContactId = 14, Login = "GrangerH", Password = "GrangerH", RoleId = 4, Birthday = "19.09.1979" });
                db.Users.Add(new User { Id = 15, Name = "Джордж", Surname = "Уизли", Sex = "М", ContactId = 15, Login = "UizlyJ", Password = "UizlyJ", RoleId = 4, Birthday = "01.04.1978" });
                db.Users.Add(new User { Id = 16, Name = "Фрэд", Surname = "Уизли", Sex = "М", ContactId = 16, Login = "UizlyF", Password = "UizlyF", RoleId = 4, Birthday = "01.04.1978" });
                db.Users.Add(new User { Id = 17, Name = "Джинни", Surname = "Уизли", Sex = "Ж", ContactId = 17, Login = "UizlyD", Password = "UizlyD", RoleId = 4, Birthday = "11.08.1981" });
                db.Users.Add(new User { Id = 18, Name = "Симус", Surname = "Финниган", Sex = "М", ContactId = 18, Login = "FinniganS", Password = "FinniganS", RoleId = 4, Birthday = "16.06.1980" });
                db.Users.Add(new User { Id = 19, Name = "Невилл", Surname = "Долгопупс", Sex = "М", ContactId = 19, Login = "DolgopupsN", Password = "DolgopupsN", RoleId = 4, Birthday = "30.07.1980" });
                db.Users.Add(new User { Id = 20, Name = "Полумна", Surname = "Лавгуд", Sex = "Ж", ContactId = 20, Login = "LavgudP", Password = "LavgudP", RoleId = 4, Birthday = "13.02.1981" });
            }
            if (!db.Categories.Any())
            {
                db.Categories.Add(new Category { Id = 1, Name = "Зельеварение" });
                db.Categories.Add(new Category { Id = 2, Name = "Защита от темных искусств" });
                db.Categories.Add(new Category { Id = 3, Name = "Прорицание" });
                db.Categories.Add(new Category { Id = 4, Name = "Магические существа" });
            }
            if (!db.Courses.Any())
            {
                db.Courses.Add(new Course { Id = 1, CategoryId = 1, DecanId = 3, Name = "Зельеварение для чайников", Format = "Онлайн (в записе)", Duration = "3 месяца", Price = 1000d, Rating = 5.0d, });
                db.Courses.Add(new Course { Id = 2, CategoryId = 1, DecanId = 3, Name = "Зельеварение для продвинутых", Format = "Онлайн (Вебинары)", Duration = "6 месяцев", Price = 2000d, Rating = 4.0d, });
                db.Courses.Add(new Course { Id = 3, CategoryId = 2, DecanId = 9, Name = "Запрещенные заклинания", Format = "Онлайн (Вебинары)", Duration = "3 месяца", Price = 1500d, Rating = 4.5d, });
                db.Courses.Add(new Course { Id = 4, CategoryId = 2, DecanId = 7, Name = "Патрунус для каждого", Format = "Онлайн (Вебинары)", Duration = "2 месяца", Price = 1100d, Rating = 5.0d, });
                db.Courses.Add(new Course { Id = 5, CategoryId = 2, DecanId = 11, Name = "Мастерство дуэлей", Format = "Онлайн (Вебинары)", Duration = "3 месяца", Price = 1500d, Rating = 4.0d, });
                db.Courses.Add(new Course { Id = 6, CategoryId = 3, DecanId = 10, Name = "Гадание по кофейным гущам", Format = "Онлайн (в записе)", Duration = "1 месяц", Price = 800d, Rating = 3.0d, });
                db.Courses.Add(new Course { Id = 7, CategoryId = 4, DecanId = 6, Name = "Полеты на гриффонах", Format = "Онлайн (Вебинары)", Duration = "1 месяц", Price = 1200d, Rating = 3.5d, });
            }
            if (!db.Lessons.Any())
            {
                db.Lessons.Add(new Lesson { Id = 1, CourseId = 1, TeacherId = 1, Name = "", Description = "", Tasks = "Сварить первое зелье" });
            }
            if (!db.Orders.Any()) 
            {
                db.Orders.Add(new Order { Id = 1, CourseId = 1, StudentId = 12, Status = "" });
            }
            if (!db.Moderations.Any())
            {

            }
            if (!db.Payments.Any())
            {

            }

            db.SaveChanges();

        }
    }
}
