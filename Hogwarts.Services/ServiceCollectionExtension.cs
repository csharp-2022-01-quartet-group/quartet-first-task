﻿using Hogwarts.Services.Notifications;
using Hogwarts.Services.RabbitMQ;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Hogwarts.Services
{
    public static class ServiceCollectionExtension
    {
        public static IServiceCollection AddHogwartsServices(this IServiceCollection services, IConfiguration Configuration)
        {
            services.AddHostedService<NotificationsHostedService>();
            services.AddSingleton<IRabbitMqService, RabbitMqService>();

            return services;
        }
    }
}
