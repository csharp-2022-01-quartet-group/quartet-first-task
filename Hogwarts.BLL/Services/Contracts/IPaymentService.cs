﻿using Hogwarts.DAL.Entities;

namespace Hogwarts.BLL.Services.Contracts
{
    public interface IPaymentService : IServiceBase<Payment>
    {
    }
}