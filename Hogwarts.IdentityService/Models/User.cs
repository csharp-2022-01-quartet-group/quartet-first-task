﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;

namespace Hogwarts.IdentityService.Models
{
    public class User : IdentityUser
    {
        [StringLength(256)]
        public string DisplayName { get; set; }
    }
}
