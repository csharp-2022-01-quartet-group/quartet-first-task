﻿using Hogwarts.DAL.Entities;
using Hogwarts.DAL.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hogwarts.DAL.Repositories.DbRepositories
{
    public class CategoryRepository : BaseRepository<Category>, ICategoryRepository
    {
        public CategoryRepository(AppDbContext context) : base(context)
        {
        }

        public Category GetByName(string categoryName)
        {
            return _dbSet.FirstOrDefault(f => f.Name == categoryName);
        }
    }
}
