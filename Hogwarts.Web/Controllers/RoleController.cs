﻿using System.Collections.Generic;
using Hogwarts.DAL.Entities;
using Hogwarts.BLL.Services;
using Hogwarts.BLL.Services.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Hogwarts.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoleController : ControllerBase
    {
        private readonly IRoleService _service;

        public RoleController(IRoleService service)
        {
            _service = service;
        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        public Role Get(long id)
        {
            return _service.Get(id);
        }

        [Authorize(Roles = "Admin")]
        [HttpGet("[action]")]
        public List<Role> AllRecord()
        {
            return _service.GetAll();
        }

        [Authorize(Roles = "Admin")]
        [HttpPut]
        public void Put(Role role)
        {
            _service.Create(role);
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public void Post(Role role)
        {
            _service.Update(role);
        }

        [Authorize(Roles = "Admin")]
        [HttpDelete]
        public void Delete(long id)
        {
            _service.Delete(id);
        }
    }
}
