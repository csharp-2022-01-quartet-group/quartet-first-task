﻿namespace Hogwarts.DAL.Entities
{
    /// <summary>
    /// Роль
    /// </summary>
    public class Role : EntityBase
    {
        /// <summary>
        /// Наименование роли
        /// </summary>
        public string Name { get; set; }

    }

}
