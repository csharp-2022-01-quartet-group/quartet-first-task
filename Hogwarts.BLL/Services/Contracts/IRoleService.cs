﻿using Hogwarts.DAL.Entities;

namespace Hogwarts.BLL.Services.Contracts
{
    public interface IRoleService : IServiceBase<Role>
    {
    }
}