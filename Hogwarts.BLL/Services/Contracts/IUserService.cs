﻿using Hogwarts.DAL.Entities;

namespace Hogwarts.BLL.Services.Contracts
{
    public interface IUserService : IServiceBase<User>
    {
        User GetByLogin(string login);
    }
}
