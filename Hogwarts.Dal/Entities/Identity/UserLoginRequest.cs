using System.ComponentModel.DataAnnotations;

namespace Hogwarts.DAL.Entities.Identity
{
    public class UserLoginRequest
    {
        [Required]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
    }
}