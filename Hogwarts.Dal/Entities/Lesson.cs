﻿
using Hogwarts.DAL.Enums;

namespace Hogwarts.DAL.Entities
{
    /// <summary>
    /// Занятие
    /// </summary>
    public class Lesson : EntityBase
    {
   /// <summary>
        /// Наименование занятия
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        /// <returns></returns>
        public string Description { get; set; }

        /// <summary>
        /// Идентификатор сущностии CourseId
        /// </summary>
        /// <returns></returns>
        public long CourseId { get; set; }

        /// <summary>
        /// Сущность курса
        /// </summary>
        public virtual Course Course { get; set; }


        /// <summary>
        /// Идентификатор сущности преподавателя
        /// </summary>
        /// <returns></returns>
        public long TeacherId { get; set; }

        /// <summary>
        /// Задания к занятию
        /// </summary>
        public string? Tasks { get; set; }


        public ELessonStatus Status { get; set; }

        /// <summary>
        /// Cущностm преподавателя
        /// </summary>
        /// <returns></returns>
        public virtual User Teacher { get; set; }

        public override string ToString() => $"{Id} - {Name}";
    }

}
