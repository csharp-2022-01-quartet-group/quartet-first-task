﻿using Hogwarts.DAL.Entities;
using Hogwarts.DAL.Repositories.Interfaces;
using Hogwarts.BLL.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hogwarts.BLL.Services
{
    public class CourseService : ServiceBase<ICourseRepository, Course>, ICourseService
    {
        public CourseService(ICourseRepository repository) : base(repository)
        {
        }

        public Course SearchName(string name)
        {
            return _repository.GetByName(name);
        }

        public override Course Get(long id)
        {
            return base.Get(id);
        }
    }
}
