﻿using Hogwarts.DAL.Entities;
using Hogwarts.DAL.Repositories.Interfaces;

namespace Hogwarts.DAL.Repositories.DbRepositories
{
    public class PaymentRepository : BaseRepository<Payment>, IPaymentRepository
    {
        public PaymentRepository(AppDbContext context) : base(context)
        {
        }
    }
}
