﻿using Hogwarts.DAL.Entities;
using Hogwarts.DAL.Repositories.Interfaces;
using Hogwarts.BLL.Services.Contracts;
using Hogwarts.BLL.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hogwarts.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly ICategoryService _service;

        public CategoryController(ICategoryService service)
        {
            _service = service;
        }

        [Authorize(Roles = "Admin,Student,Teacher")]
        [HttpGet]
        public Category Get(long id)
        {
            return _service.Get(id);
        }

        [Authorize(Roles = "Admin,Student,Teacher")]
        [HttpGet("[action]")]
        public List<Category> AllRecord()
        {
            return _service.GetAll();
        }

        [Authorize(Roles = "Admin,Student,Teacher")]
        [HttpGet("[action]")]
        public Category SearchName(string categoryName)
        {
            var res = _service.SearchName(categoryName);

            return res;
        }

        [Authorize(Roles = "Admin")]
        [HttpPut]
        public void Put(Category category)
        {
            _service.Create(category);
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public void Post(Category category)
        {
            _service.Update(category);
        }

        [Authorize(Roles = "Admin")]
        [HttpDelete]
        public void Delete(long id)
        {
            _service.Delete(id);
        }
    }
}
