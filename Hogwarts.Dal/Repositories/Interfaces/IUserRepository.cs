﻿using Hogwarts.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hogwarts.DAL.Repositories.Interfaces
{
    public interface IUserRepository : IBaseRepository<User>
    {
        User GetByLogin(string login);
    }
}
