﻿using System.Threading.Tasks;

namespace Hogwarts.Services.RabbitMQ
{
    public interface IRabbitMqService
    {
        void SendMessage(object obj);
        void SendMessage(string message);
    }
}
