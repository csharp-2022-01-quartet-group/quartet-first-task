﻿using Hogwarts.DAL.Entities;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Hogwarts.BLL.Services;
using Hogwarts.BLL.Services.Contracts;
using Microsoft.AspNetCore.Authorization;

namespace Hogwarts.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LessonController : ControllerBase
    {
        private readonly ILessonService _service;

        public LessonController(ILessonService service)
        {
            _service = service;
        }

        [Authorize(Roles = "Admin,Student,Teacher")]
        [HttpGet]
        public Lesson Get(long id)
        {
            return _service.Get(id);
        }

        [Authorize(Roles = "Admin,Student,Teacher")]
        [HttpGet("[action]")]
        public List<Lesson> AllRecord()
        {
            return _service.GetAll();
        }

        [Authorize(Roles = "Admin,Student,Teacher")]
        [HttpGet("[action]")]
        public Lesson SearchName(string name)
        {
            return _service.SearchName(name);
        }

        [Authorize(Roles = "Admin,Teacher")]
        [HttpPut]
        public void Put(Lesson lesson)
        {
            _service.Create(lesson);
        }

        [Authorize(Roles = "Admin,Teacher")]
        [HttpPost]
        public void Post(Lesson lesson)
        {
            _service.Update(lesson);
        }

        [Authorize(Roles = "Admin,Teacher")]
        [HttpDelete]
        public void Delete(long id)
        {
            _service.Delete(id);
        }

        [Authorize(Roles = "Admin,Student,Teacher")]
        [HttpGet("[action]")]
        public int GetMinutes(long id)
        {
            return _service.CalculateMinutes(id);
        }
    }
}
