﻿# Создает docker-образ для проекта ✨M"Обучающая платформа Hogwarts"✨

####  Создаем образ с RabbitMQ c настроенными под проект привязкой, очередью и обменником.

## Создание образа:

```sh
docker build -t <youruser>/hogwarts-rabbitmq .
```

## Запуск образа

```sh
docker run -p 15672:15672 -p 5672:5672 <youruser>/hogwarts-rabbitmq
```
## Готовый образ
https://hub.docker.com/repository/docker/postmanyardocker/hogwarts-rabbitmq