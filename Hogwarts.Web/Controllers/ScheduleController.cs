﻿using System.Collections.Generic;
using AutoMapper;
using Hogwarts.DAL.DTO;
using Hogwarts.DAL.Entities;
using Hogwarts.BLL.Services;
using Hogwarts.BLL.Services.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Hogwarts.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ScheduleController : ControllerBase
    {
        private readonly IScheduleService _service;
        private readonly IMapper _mapper;

        public ScheduleController(IScheduleService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        [Authorize(Roles = "Admin,Student,Teacher")]
        [HttpGet("[action]")]
        public List<Schedule> All()
        {
            return _service.GetAll();
        }

        [Authorize(Roles = "Admin,Teacher")]
        [HttpPut]
        public void Put(ScheduleDTO schedule)
        {
            _service.Create(_mapper.Map<Schedule>(schedule));
        }

        [Authorize(Roles = "Admin,Teacher")]
        [HttpPost]
        public void Post(ScheduleDTO schedule)
        {
            _service.Update(_mapper.Map<Schedule>(schedule));
        }

        [Authorize(Roles = "Admin,Teacher")]
        [HttpDelete]
        public void Delete(long id)
        {
            _service.Delete(id);
        }
    }
}
