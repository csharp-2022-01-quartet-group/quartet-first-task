﻿using System.Collections.Generic;

namespace Hogwarts.DAL.Entities
{
    /// <summary>
    /// Курс
    /// </summary>
    public class Course : EntityBase
    {
        /// <summary>
        /// Наименование курса
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Описание курса
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Идентификатор сущности декана
        /// </summary>
        public long? DecanId { get; set; }

        /// <summary>
        /// Сущность декана
        /// </summary>
        public virtual User? Decan { get; set; }

        /// <summary>
        /// Цена курса
        /// </summary>
        public double Price { get; set; }

        /// <summary>
        /// Идентификатор сущности категории
        /// </summary>
        public long CategoryId { get; set; }

        /// <summary>
        /// Статус модерации
        /// </summary>
        public bool Status { get; set; }

        /// <summary>
        /// Рейтинг курса
        /// </summary>
        public double Rating { get; set; }

        /// <summary>
        /// Продолжительность курса
        /// </summary>
        public string Duration { get; set; }

        /// <summary>
        /// Формат курса
        /// </summary>
        public string Format { get; set; }

        /// <summary>
        /// Список навыков
        /// </summary>
        public List<string> SkillsList { get; set; }

        /// <summary>
        /// Список занятий
        /// </summary>
        public List<string> LessonsList { get; set; }


        public virtual List<Lesson> Lessons { get; set; }

        /// <summary>
        /// Сущность категории
        /// </summary>
        public virtual Category Category { get; set; }

        //public override string ToString()
        //{
        //    return $"{CourseId}, {CategoryId}, {LessonId}";
        //}
    }

}
