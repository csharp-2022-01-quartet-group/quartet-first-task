﻿using Hogwarts.DAL.Entities;
using Hogwarts.DAL.Repositories.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace Hogwarts.DAL.Repositories.DbRepositories
{
    public class LessonRepository : BaseRepository<Lesson>, ILessonRepository
    {
        public LessonRepository(AppDbContext context) : base(context)
        {
        }

        public Lesson GetByName(string name)
        {
            return _dbSet.FirstOrDefault(f => f.Name == name);
        }

        public List<Lesson> GetLessonsByCourse(long courseId)
        {
            return _dbSet.Where(x=>x.CourseId == courseId).ToList();
        }
    }
}
