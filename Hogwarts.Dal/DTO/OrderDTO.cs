﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hogwarts.DAL.DTO
{
    public class OrderDTO
    {
        public long StudentId { get; set; }

        public long CourseId { get; set; }

        public string Status { get; set; }
    }
}
