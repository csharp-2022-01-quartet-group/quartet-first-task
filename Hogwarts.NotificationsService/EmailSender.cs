using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;


namespace Hogwarts.NotificationsService
{
    public class EmailSender
    {
        private readonly IConfiguration _configuration;
        public EmailSender(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void SendEmail(Message message)
        {
            SendEmail(message.Body, message.EmailTo, message.Subject);
        }

        public async void SendEmail(string body, string mailTo, string subject, IEnumerable<Attachment> attachments = null)
        {
            var from = new MailAddress(_configuration["EmailSender:UserName"], "Хогвардс");
            var to = new MailAddress(mailTo);
            var m = new MailMessage(from, to)
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = false
            };

            if(attachments != null)
            {
                foreach (var attachment in attachments)
                {
                    m.Attachments.Add(attachment);
                }
            }

            var smtp = new SmtpClient(_configuration["EmailSender:Host"], int.Parse(_configuration["EmailSender:Port"]))
            {
                Credentials = new NetworkCredential(
                    _configuration["EmailSender:UserName"],
                    _configuration["EmailSender:Password"]
                ),
                EnableSsl = true
            };
            await smtp.SendMailAsync(m);
        }
    }
}
