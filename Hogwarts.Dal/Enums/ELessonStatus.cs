﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hogwarts.DAL.Enums
{
    public enum ELessonStatus
    {
        Empty = 0,
        Completed = 1,
        NotCompleted = 2
    }
}
