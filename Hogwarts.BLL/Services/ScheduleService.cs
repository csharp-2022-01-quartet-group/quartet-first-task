﻿using System;
using System.Collections.Generic;
using Hogwarts.DAL.DTO;
using Hogwarts.DAL.Entities;
using Hogwarts.DAL.Repositories.Interfaces;
using Hogwarts.BLL.Services.Contracts;

namespace Hogwarts.BLL.Services
{
    public class ScheduleService : ServiceBase<IScheduleRepository, Schedule>, IScheduleService
    {
        public ScheduleService(IScheduleRepository repository) : base(repository)
        {
        }

        public List<Message> GetMessages(DateTime dateTime)
        {
            return _repository.GetMessages(dateTime);
        }
    }
}
