﻿using Hogwarts.DAL.Entities;
using Hogwarts.DAL.Repositories.Interfaces;

namespace Hogwarts.DAL.Repositories.DbRepositories
{
    public class ContactRepository : BaseRepository<Contact>, IContactRepository
    {
        public ContactRepository(AppDbContext context) : base(context)
        {
        }
    }
}
