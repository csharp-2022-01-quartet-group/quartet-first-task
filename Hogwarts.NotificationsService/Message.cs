﻿namespace Hogwarts.NotificationsService
{
    public class Message
    {
        public string EmailTo { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
    }
}
