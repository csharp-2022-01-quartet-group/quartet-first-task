
namespace Hogwarts.DAL.Entities
{
    /// <summary>
    /// �����
    /// </summary>
    public class Order : EntityBase
    {
        
        /// <summary>
        /// ������������� �������� ���������� �����
        /// </summary>
        public long StudentId{ get; set; }

        /// <summary>
        /// �������� �������� ���������� �����
        /// </summary>
        public virtual User Student { get; set; }

        /// <summary>
        /// ������������� �����
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// �������� �����
        /// </summary>
        public virtual Course Course { get; set; }

        /// <summary>
        /// ����������� ������ ������ 
        /// </summary>
        public string Status { get; set; }

    }

}
