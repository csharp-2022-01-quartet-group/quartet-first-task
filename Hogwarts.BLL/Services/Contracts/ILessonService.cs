﻿using Hogwarts.DAL.Entities;

namespace Hogwarts.BLL.Services.Contracts
{
    public interface ILessonService : IServiceBase<Lesson>
    {
        Lesson SearchName(string name);
        void Update(Lesson lesson);
        int CalculateMinutes(long id);
    }
}