﻿namespace Hogwarts.DAL.Entities.Identity
{
    public enum UserRoles
    {
        None,
        Admin,
        User,
        Student,
        Teacher
    }
}
