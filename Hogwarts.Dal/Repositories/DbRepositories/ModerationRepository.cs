﻿using Hogwarts.DAL.Entities;
using Hogwarts.DAL.Repositories.Interfaces;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hogwarts.DAL.Repositories.DbRepositories
{
    public class ModerationRepository : BaseRepository<Moderation>, IModerationRepository
    {
        public ModerationRepository(AppDbContext context) : base(context)
        {
        }
    }
}
