using System.ComponentModel.DataAnnotations;

namespace Hogwarts.DAL.Entities.Identity
{
    public class UserRegistrationRequest
    {
        [Required]
        public string Username { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
    }
}