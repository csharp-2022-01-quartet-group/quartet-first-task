﻿using Hogwarts.DAL.Entities;

namespace Hogwarts.BLL.Services.Contracts
{
    public interface ICategoryService : IServiceBase<Category>
    {
        Category SearchName(string userName);
    }
}