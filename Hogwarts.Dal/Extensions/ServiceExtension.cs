﻿using Hogwarts.DAL.Repositories.DbRepositories;
using Hogwarts.DAL.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Hogwarts.DAL.Extensions
{
    public static class ServiceExtension
    {
        public static void ConfigurationContext(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<AppDbContext>(option =>
            {
                option
                    .UseLazyLoadingProxies()
                    .UseNpgsql(configuration.GetSection("ConnectionStrings")["Postgresql"]);
            });
        }

        public static void ConfigurationRepositories(this IServiceCollection services)
        {
            services.AddScoped<ICategoryRepository, CategoryRepository>();
            services.AddScoped<IContactRepository, ContactRepository>();
            services.AddScoped<ICourseRepository, CourseRepository>();
            services.AddScoped<ILessonRepository, LessonRepository>();
            services.AddScoped<IOrderRepository, OrderRepository>();
            services.AddScoped<IPaymentRepository, PaymentRepository>();
            services.AddScoped<IRoleRepository, RoleRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IModerationRepository, ModerationRepository>();
            services.AddScoped<IScheduleRepository, ScheduleRepository>();
            services.AddScoped<IWrapperRepository, WrapperRepository>();
        }
    }
}
