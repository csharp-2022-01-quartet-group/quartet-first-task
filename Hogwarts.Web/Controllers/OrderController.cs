﻿using Hogwarts.DAL.Entities;
using Hogwarts.BLL.Services;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using AutoMapper;
using Hogwarts.DAL.DTO;
using Hogwarts.BLL.Services.Contracts;
using Microsoft.AspNetCore.Authorization;

namespace Hogwarts.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly IOrderService _service;
        private readonly IMapper _mapper;

        public OrderController(IOrderService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        [Authorize(Roles = "Admin,Student,Teacher")]
        [HttpGet]
        public Order Get(long id)
        {
            return _service.Get(id);
        }

        [Authorize(Roles = "Admin,Student,Teacher")]
        [HttpGet("[action]")]
        public List<Order> AllRecord()
        {
            return _service.GetAll();
        }

        [Authorize(Roles = "Admin,Student,Teacher")]
        [HttpPut]
        public void Put(OrderDTO order)
        {
            _service.Create(_mapper.Map<Order>(order));
        }

        [Authorize(Roles = "Admin,Student,Teacher")]
        [HttpPost]
        public void Post(OrderDTO order)
        {
            _service.Update(_mapper.Map<Order>(order));
        }

        [Authorize(Roles = "Admin,Student,Teacher")]
        [HttpDelete]
        public void Delete(long id)
        {
            _service.Delete(id);
        }
    }
}
