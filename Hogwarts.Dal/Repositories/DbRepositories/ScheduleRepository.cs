﻿using Hogwarts.DAL.DTO;
using Hogwarts.DAL.Entities;
using Hogwarts.DAL.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Hogwarts.DAL.Repositories.DbRepositories
{
    public class ScheduleRepository : BaseRepository<Schedule>, IScheduleRepository
    {
        private readonly AppDbContext _context;
        public ScheduleRepository(AppDbContext context) : base(context)
        {
            _context = context;
        }

        public List<Message> GetMessages(DateTime dateTime)
        {

            var messages = from schedule in _context.Schedules
                           where schedule.LessonDateTime > dateTime && schedule.LessonDateTime < dateTime.AddHours(3)
                           join course in _context.Courses on schedule.Lesson.CourseId equals course.Id
                           join orders in _context.Orders on course.Id equals orders.CourseId
                           join payments in _context.Payments on orders.Id equals payments.OrderId
                           join users in _context.Users on orders.StudentId equals users.Id
                           select new Message
                           {
                               EmailTo = users.Contact.Email,
                               Subject = $"Напоминаем вам, что {schedule.LessonDateTime:f} состоится занятие по курсу {course.Name}",
                               Body = $"Урок: {schedule.Lesson.Name}"
                           };

            return messages.ToList();

        }
    }
}
