﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Hogwarts.Web.Controllers
{
    
    [Route("api/[controller]")]
    [ApiController]
    public class TestController : ControllerBase
    {

        [Authorize(Roles = "User")]
        [HttpGet("[action]")]
        public IActionResult TestUserRole()
        {
            return Ok("Role User");
        }

        [Authorize(Roles = "Admin")]
        [HttpGet("[action]")]
        public IActionResult TestAdminRole()
        {
            return Ok("Role Admin");
        }
    }
}
