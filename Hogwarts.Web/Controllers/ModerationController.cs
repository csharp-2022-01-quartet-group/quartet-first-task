﻿using Hogwarts.DAL.Entities;
using Hogwarts.DAL.Repositories.Interfaces;
using Hogwarts.BLL.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Hogwarts.BLL.Services.Contracts;
using Microsoft.AspNetCore.Authorization;

namespace Hogwarts.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ModerationController : ControllerBase
    {
        private readonly IModerationService _service;

        public ModerationController(IModerationService service)
        {
            _service = service;
        }

        [Authorize(Roles = "Admin,Teacher")]
        [HttpGet]
        public Moderation Get(long id)
        {
            return _service.Get(id);
        }

        [Authorize(Roles = "Admin,Teacher")]
        [HttpGet("[action]")]
        public List<Moderation> AllUser()
        {
            return _service.GetAll();
        }

        [Authorize(Roles = "Admin")]
        [HttpPut]
        public void Put(Moderation moderation)
        {
            _service.Create(moderation);
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public void Post(Moderation moderation)
        {
            _service.Update(moderation);
        }

        [Authorize(Roles = "Admin")]
        [HttpDelete]
        public void Delete(long id)
        {
            _service.Delete(id);
        }
    }
}
