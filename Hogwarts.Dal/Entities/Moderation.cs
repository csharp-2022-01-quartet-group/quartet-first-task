﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hogwarts.DAL.Entities
{
    /// <summary>
    /// Модерация
    /// </summary>
    public class Moderation : EntityBase
    {
        /// <summary>
        /// Id лекции
        /// </summary>
        public long LessonId { get; set; }
        /// <summary>
        /// Id пользователя
        /// </summary>
        public long UserId { get; set; }
        /// <summary>
        /// Комментарии
        /// </summary>
        public string Comments { get; set; }
        /// <summary>
        /// Время создания модерации
        /// </summary>
        public DateTime CreateTimeUTC { get; set; }
    }
}
