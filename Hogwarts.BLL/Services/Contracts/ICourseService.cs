﻿using Hogwarts.DAL.Entities;

namespace Hogwarts.BLL.Services.Contracts
{
    public interface ICourseService : IServiceBase<Course>
    {
        Course SearchName(string name);
    }
}