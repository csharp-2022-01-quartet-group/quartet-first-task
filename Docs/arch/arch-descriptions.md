# Описание архитектурных решений

1. [Диаграмма классов](#class-diagrams)
2. [Описание сервисов](#services)
3. [Аутентификация](#auth)

## Диаграмма классов <a name="class-diagrams"></a>
> Under construction ...

## Описание сервисов <a name="services"></a>
> Описание сервисов в swagger формате. 
> Under construction ...

## Решение по аутенфикации <a name="auth"></a>

![](imgs/auth-service.png)

> Under construction ...
