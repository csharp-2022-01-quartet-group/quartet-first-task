﻿using Hogwarts.DAL.Entities;
using Hogwarts.DAL.Repositories.Interfaces;
using Hogwarts.BLL.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hogwarts.BLL.Services
{
    public class CategoryService : ServiceBase<ICategoryRepository, Category>, ICategoryService
    {
        public CategoryService(ICategoryRepository repository) : base(repository)
        {
        }

        public Category SearchName(string name)
        {
            return _repository.GetByName(name);
        }
    }
}
