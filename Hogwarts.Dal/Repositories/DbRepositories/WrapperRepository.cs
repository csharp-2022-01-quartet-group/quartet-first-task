﻿using Hogwarts.DAL.Entities;
using Hogwarts.DAL.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hogwarts.DAL.Repositories.DbRepositories
{
    public class WrapperRepository : IWrapperRepository
    {
        private readonly AppDbContext _appDbContext;

        public WrapperRepository(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        private ICategoryRepository _categoryRepository;
        private IContactRepository _contactRepository;
        private ICourseRepository _courseRepository;
        private ILessonRepository _lessonRepository;
        private IOrderRepository _orderRepository;
        private IPaymentRepository _paymentRepository;
        private IRoleRepository _roleRepository;
        private IUserRepository _userRepository;
        private IModerationRepository _moderationRepository;
        private IScheduleRepository _scheduleRepository;

        public ICategoryRepository CategoryRepository
        {
            get
            {
                if (_categoryRepository == null)
                {
                    _categoryRepository = new CategoryRepository(_appDbContext);
                }

                return _categoryRepository;
            }       
        }

        public IContactRepository ContactRepository
        {
            get
            {
                if (_contactRepository == null)
                {
                    _contactRepository = new ContactRepository(_appDbContext);
                }

                return _contactRepository;
            }
        }

        public ICourseRepository CourseRepository
        {
            get
            {
                if (_courseRepository == null)
                {
                    _courseRepository = new CourseRepository(_appDbContext);
                }

                return _courseRepository;
            }
        }

        public ILessonRepository LessonRepository
        {
            get
            {
                if (_lessonRepository == null)
                {
                    _lessonRepository = new LessonRepository(_appDbContext);
                }

                return _lessonRepository;
            }
        }

        public IOrderRepository OrderRepository
        {
            get
            {
                if (_orderRepository == null)
                {
                    _orderRepository = new OrderRepository(_appDbContext);
                }

                return _orderRepository;
            }
        }

        public IPaymentRepository PaymentRepository
        {
            get
            {
                if (_paymentRepository == null)
                {
                    _paymentRepository = new PaymentRepository(_appDbContext);
                }

                return _paymentRepository;
            }
        }

        public IRoleRepository RoleRepository
        {
            get
            {
                if (_roleRepository == null)
                {
                    _roleRepository = new RoleRepository(_appDbContext);
                }

                return _roleRepository;
            }
        }

        public IUserRepository UserRepository
        {
            get
            {
                if (_userRepository == null)
                {
                    _userRepository = new UserRepository(_appDbContext);
                }

                return _userRepository;
            }
        }

        public IModerationRepository ModerationRepository
        {
            get
            {
                if (_moderationRepository == null)
                {
                    _moderationRepository = new ModerationRepository(_appDbContext);
                }

                return _moderationRepository;
            }
        }

        public IScheduleRepository ScheduleRepository
        {
            get
            {
                if (_scheduleRepository == null)
                {
                    _scheduleRepository = new ScheduleRepository(_appDbContext);
                }

                return _scheduleRepository;
            }
        }

        public void SaveChange()
        {
            _appDbContext.SaveChanges();
        }

        public async Task SaveChangeAsync()
        {
            await _appDbContext.SaveChangesAsync();
        }
    }
}
