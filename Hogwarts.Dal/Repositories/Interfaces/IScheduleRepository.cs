﻿using System;
using System.Collections.Generic;
using Hogwarts.DAL.Entities;
using Hogwarts.DAL.DTO;

namespace Hogwarts.DAL.Repositories.Interfaces
{
    public interface IScheduleRepository : IBaseRepository<Schedule>
    {
        public List<Message> GetMessages(DateTime dateTime);
    }
}
