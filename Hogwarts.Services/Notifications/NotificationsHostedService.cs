﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Hogwarts.DAL;
using Hogwarts.DAL.DTO;
using Hogwarts.DAL.Repositories.DbRepositories;
using Hogwarts.Services.RabbitMQ;
using Hogwarts.BLL.Services;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Hogwarts.Services.Notifications
{
    public class NotificationsHostedService : IHostedService
    {
        private Timer _timer;
        private readonly ScheduleService _scheduleService;
        private readonly IServiceScopeFactory _serviceScopeFactory;
        private readonly IRabbitMqService _mqService;

        public NotificationsHostedService(IServiceScopeFactory serviceScopeFactory, IRabbitMqService mqService)
        {
            _mqService = mqService;
            _serviceScopeFactory = serviceScopeFactory;
            var serviceProvider = _serviceScopeFactory
                .CreateScope()
                .ServiceProvider;
            var appDbContext = serviceProvider.GetRequiredService<AppDbContext>();
            
            _scheduleService = new ScheduleService(new ScheduleRepository(appDbContext));
        }

        Task IHostedService.StartAsync(CancellationToken cancellationToken)
        {
            //_timer = new Timer(HandleNotifications, null, TimeSpan.FromHours(3), TimeSpan.FromHours(3));
            //TODO: 1 минута для отладки
            _timer = new Timer(HandleNotifications, null, TimeSpan.FromMinutes(1), TimeSpan.FromMinutes(1));
            return Task.CompletedTask;
        }

        private async void HandleNotifications(object o)
        {
            var notifications = _scheduleService.GetMessages(DateTime.Now);
            if (notifications.Any()) 
                await Send(notifications);
        }

        private async Task Send(IList<Message> notifications)
        {
            foreach (var n in notifications)
            {
                await Task.Factory.StartNew(()=>_mqService.SendMessage(n));
            }
        }

        Task IHostedService.StopAsync(CancellationToken cancellationToken)
        {
            _timer?.Change(Timeout.Infinite, 0);
            return Task.CompletedTask;
        }
    }
}
