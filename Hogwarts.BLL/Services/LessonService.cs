﻿using Hogwarts.DAL.Entities;
using Hogwarts.DAL.Repositories.Interfaces;
using Hogwarts.BLL.Services.Contracts;
using System.Linq;

namespace Hogwarts.BLL.Services
{
    public class LessonService : ServiceBase<ILessonRepository, Lesson>, ILessonService
    {
        private readonly ICourseService _courseService;
        public LessonService(ILessonRepository repository, ICourseService courseService) : base(repository)
        {
            _courseService = courseService;
        }

        public int CalculateMinutes(long id)
        {
            throw new System.NotImplementedException();
        }

        public Lesson SearchName(string name)
        {
            return _repository.GetByName(name);
        }
        public override void Update(Lesson lesson)
        {
            bool isChangeCourseStatus = false;

            var lessonDb = Get(lesson.Id);

            if (lessonDb!=null && lessonDb.Status != lesson.Status)
            {
                isChangeCourseStatus = true;
            }

            Update(lesson);

            if (isChangeCourseStatus)
                ChangeCourseStatus(lesson);
        }
        private void ChangeCourseStatus(Lesson lesson)
        {
            var course = lesson.Course;
            var lesssons = _repository.GetLessonsByCourse(course.Id);
            var notCompletedLessons = lesssons.Where(x => x.Status != Hogwarts.DAL.Enums.ELessonStatus.Completed).ToList();
            if (notCompletedLessons?.Count == 0)
            {
                course.Status = true;
                _courseService.Update(course);
            }
        }
    }
}
