﻿using Hogwarts.DAL.Entities;
using Hogwarts.DAL.Repositories.Interfaces;
using Hogwarts.BLL.Services.Contracts;

namespace Hogwarts.BLL.Services
{
    public class UserService : ServiceBase<IUserRepository, User>, IUserService
    {
        public UserService(IUserRepository repository) : base(repository)
        {
        }

        public User GetByLogin(string login)
        {
            return _repository.GetByLogin(login);
        }
    }
}
