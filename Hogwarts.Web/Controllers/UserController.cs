﻿using Hogwarts.DAL.Entities;
using Hogwarts.DAL.Repositories.Interfaces;
using Hogwarts.BLL.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Hogwarts.BLL.Services.Contracts;
using Microsoft.AspNetCore.Authorization;

namespace Hogwarts.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _service;

        public UserController(IUserService service)
        {
            _service = service;
        }

        [Authorize(Roles = "Admin,Student,Teacher")]
        [HttpGet]
        public User Get(long id)
        {
            return _service.Get(id);
        }

        [Authorize(Roles = "Admin,Student,Teacher")]
        [HttpGet("[action]")]
        public List<User> AllUser()
        {
            return _service.GetAll();
        }

        [HttpGet("[action]")]
        public User SearchUser(string login)
        {
            return _service.GetByLogin(login);
        }

        [Authorize(Roles = "Admin,Student,Teacher")]
        [HttpPut]
        public void Put(User user)
        {
            _service.Create(user);
        }

        [HttpPost]
        public void Post(User user)
        {
            _service.Update(user);
        }

        [HttpDelete]
        public void Delete(long id)
        {
            _service.Delete(id);
        }
    }
}
