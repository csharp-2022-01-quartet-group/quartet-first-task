# PostgreSQL

## Оглавление:
* [Параметры соединения](#Database-credentials)
* [Установка postgreSql в Heroku](#Установка-postgreSql)

## Параметры соединения <a name="Database-credentials"></a>

|   |   |
|---|---|
|__Host__| ec2-63-34-153-52.eu-west-1.compute.amazonaws.com |
|__Database__| d1norfp62ua6hs |
|__User__| opvebjjezfdceu |
|__Port__| 5432 |
|__Password__| dd4484647b533c8c20338d2cec9ea3fc558f026cabd49e2ec6bd5a5ebd2af3b3 |
|__URI__| postgres://opvebjjezfdceu:dd4484647b533c8c20338d2cec9ea3fc558f026cabd49e2ec6bd5a5ebd2af3b3@ec2-63-34-153-52.eu-west-1.compute.amazonaws.com:5432/d1norfp62ua6hs |
|__Heroku CLI__| heroku pg:psql postgresql-concave-60972 --app otus-c01 |

Пример соединения
![](imgs/connection-example.png)


## Установка postgreSql в Heroku <a name="Установка-postgreSql"></a>
>**Note**: Показан один из путей установки PostgreSQL на Heroku. Этого же рузультата можно добиться используя другие пути меню.

1. Создаем пользователя на https://signup.heroku.com/login
![](imgs/login.png)


2. Подтверждаем создание аккаунта
![](imgs/check-your-email.png)


3. Устанавливаем пароль
![](imgs/set-password.png)


4. Попадаем на Page not found. Нажимаем на логотип Heroku, возвращаемся на ashboard
![](imgs/page-not-found.png)


5. Соглашаемся с Terms of Service
![](imgs/terms-of-service.png)


6. Создаем новое приложение
![](imgs/create-new-app.png)


7. Даем ему имя и выбираем регион
![](imgs/create-new-app-2.png)


8. Переходим на закладку Resources и выбираем add-on Heroku PostgreSQL
![](imgs/add-resource.png)


9. Подтверждаем создание базы данных с бесплатным планом "Hobby Dev - Free"
![](imgs/heroku-postgresql-creation.png)


10. База создана. Перезодим на страницу Database 
![](imgs/database-created.png)


11. На странице базы данных переходим на закладку Settings
![](imgs/database-page.png)


12. Нажимаем кнопку "View Credentials"
![](imgs/settings-page.png)


13. Видим параметры соединения
![](imgs/database-credentials.png)


14. Документация по Heroku Postgres
https://devcenter.heroku.com/articles/heroku-postgresql
