﻿using System;

namespace Hogwarts.DAL.Entities
{
    public class Schedule : EntityBase
    {
        /// <summary>
        /// Дата время урока
        /// </summary>
        public DateTime LessonDateTime { get; set; }
        
        /// <summary>
        /// Идентификатор урока
        /// </summary>
        public long LessonId { get; set; }
        
        /// <summary>
        /// Урок
        /// </summary>
        public virtual Lesson Lesson { get; set; }
    }
}
