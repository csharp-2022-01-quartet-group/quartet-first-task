﻿using Microsoft.Extensions.DependencyInjection;
using Hogwarts.BLL.Services;
using Hogwarts.BLL.Services.Contracts;
using Hogwarts.BLL.Mapping;

namespace Hogwarts.BLL.Extensions
{
    public static class ServiceExtension
    {
        public static void ConfigurationServices(this IServiceCollection services)
        {
            services.AddScoped<ICategoryService, CategoryService>();
            services.AddScoped<IContactService, ContactService>();
            services.AddScoped<ICourseService, CourseService>();
            services.AddScoped<ILessonService, LessonService>();
            services.AddScoped<IOrderService, OrderService>();
            services.AddScoped<IPaymentService, PaymentService>();
            services.AddScoped<IRoleService, RoleService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IModerationService, ModerationService>();
            services.AddScoped<IScheduleService, ScheduleService>();
        }

        public static void ConfigurationAutoMapper(this IServiceCollection services)
        {
            services.AddAutoMapper(typeof(MappingProfile));
        }
    }
}
