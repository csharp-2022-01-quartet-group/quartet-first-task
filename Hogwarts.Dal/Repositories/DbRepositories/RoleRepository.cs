﻿using Hogwarts.DAL.Entities;
using Hogwarts.DAL.Repositories.Interfaces;

namespace Hogwarts.DAL.Repositories.DbRepositories
{
    public class RoleRepository : BaseRepository<Role>, IRoleRepository
    {
        public RoleRepository(AppDbContext context) : base(context)
        {
        }
    }
}
