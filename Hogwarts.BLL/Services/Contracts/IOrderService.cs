﻿using Hogwarts.DAL.Entities;

namespace Hogwarts.BLL.Services.Contracts
{
    public interface IOrderService : IServiceBase<Order>
    {
    }
}