﻿using System.Collections.Generic;
using Hogwarts.DAL.Entities;
using Hogwarts.BLL.Services;
using Hogwarts.BLL.Services.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Hogwarts.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CourseController : ControllerBase
    {
        private readonly ICourseService _service;

        public CourseController(ICourseService service)
        {
            _service = service;
        }

        [Authorize(Roles = "Admin,Student,Teacher")]
        [HttpGet]
        public Course Get(long id)
        {
            return _service.Get(id);
        }

        [Authorize(Roles = "Admin,Student,Teacher")]
        [HttpGet("[action]")]
        public List<Course> AllRecord()
        {
            return _service.GetAll();
        }

        [Authorize(Roles = "Admin,Student,Teacher")]
        [HttpGet("[action]")]
        public Course SearchName(string name)
        {
            return _service.SearchName(name);
        }


        [Authorize(Roles = "Admin,Teacher")]
        [HttpPut]
        public void Put(Course course)
        {
            _service.Create(course);
        }

        [Authorize(Roles = "Admin,Teacher")]
        [HttpPost]
        public void Post(Course course)
        {
            _service.Update(course);
        }


        [Authorize(Roles = "Admin,Teacher")]
        [HttpDelete]
        public void Delete(long id)
        {
            _service.Delete(id);
        }

    }
}
