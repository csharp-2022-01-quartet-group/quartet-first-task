﻿using Hogwarts.DAL.Entities;
using Hogwarts.DAL.Repositories.Interfaces;
using Hogwarts.BLL.Services.Contracts;

namespace Hogwarts.BLL.Services
{
    public class ModerationService: ServiceBase<IModerationRepository, Moderation>, IModerationService
    {
        public ModerationService(IModerationRepository repository) : base(repository)
    {
    }
}
}
