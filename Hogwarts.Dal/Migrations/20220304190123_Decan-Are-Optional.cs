﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Hogwarts.DAL.Migrations
{
    public partial class DecanAreOptional : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Courses_Users_DecanId",
                table: "Courses");

            migrationBuilder.AlterColumn<long>(
                name: "DecanId",
                table: "Courses",
                type: "bigint",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint");

            migrationBuilder.AddForeignKey(
                name: "FK_Courses_Users_DecanId",
                table: "Courses",
                column: "DecanId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Courses_Users_DecanId",
                table: "Courses");

            migrationBuilder.AlterColumn<long>(
                name: "DecanId",
                table: "Courses",
                type: "bigint",
                nullable: false,
                defaultValue: 0L,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Courses_Users_DecanId",
                table: "Courses",
                column: "DecanId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
