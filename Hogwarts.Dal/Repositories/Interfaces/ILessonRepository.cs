﻿using Hogwarts.DAL.Entities;
using System.Collections.Generic;

namespace Hogwarts.DAL.Repositories.Interfaces
{
    public interface ILessonRepository : IBaseRepository<Lesson>
    {
        Lesson GetByName(string name);
        List<Lesson> GetLessonsByCourse(long courseId);
    }
}
