﻿using Hogwarts.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hogwarts.DAL.Repositories.Interfaces
{
    public interface IWrapperRepository
    {
        ICategoryRepository CategoryRepository { get;  }
        IContactRepository ContactRepository { get; }
        ICourseRepository CourseRepository { get; }
        ILessonRepository LessonRepository { get; }
        IOrderRepository OrderRepository { get; }
        IPaymentRepository PaymentRepository { get; }
        IRoleRepository RoleRepository { get;  }
        IUserRepository UserRepository { get;  }
        IModerationRepository ModerationRepository { get; }
        IScheduleRepository ScheduleRepository { get; }

        Task SaveChangeAsync();

        void SaveChange();
    }
}
