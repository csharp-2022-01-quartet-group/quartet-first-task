﻿using Hogwarts.DAL.Entities;
using System.Collections.Generic;

namespace Hogwarts.BLL.Services.Contracts
{
    public interface IServiceBase<T> 
        where T : EntityBase
    {
        void Create(T entity);
        void Delete(long id);
        List<T> Get(IEnumerable<long> ids);
        T Get(long id);
        List<T> GetAll();
        void Update(T entity);
    }
}