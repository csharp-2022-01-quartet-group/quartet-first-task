﻿using System.Collections.Generic;
using Hogwarts.DAL.Entities;
using Hogwarts.BLL.Services;
using Hogwarts.BLL.Services.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Hogwarts.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PaymentController : ControllerBase
    {
        private readonly IPaymentService _service;

        public PaymentController(IPaymentService service)
        {
            _service = service;
        }

        [Authorize(Roles = "Admin,Student,Teacher")]
        [HttpGet]
        public Payment Get(long id)
        {
            return _service.Get(id);
        }

        [Authorize(Roles = "Admin,Student,Teacher")]
        [HttpGet("[action]")]
        public List<Payment> AllRecord()
        {
            return _service.GetAll();
        }

        [Authorize(Roles = "Admin")]
        [HttpPut]
        public void Put(Payment payment)
        {
            _service.Create(payment);
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public void Post(Payment payment)
        {
            _service.Update(payment);
        }

        [Authorize(Roles = "Admin")]
        [HttpDelete]
        public void Delete(long id)
        {
            _service.Delete(id);
        }
    }
}
