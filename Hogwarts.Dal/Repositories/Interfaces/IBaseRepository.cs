﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Hogwarts.DAL.Repositories.Interfaces
{
    public interface IBaseRepository<T>
        where T : class
    {
        T Get(long id);
        IEnumerable<T> Get(IEnumerable<long> ids);
        IEnumerable<T> GetAll();
        void Create(T entity);
        void Update(T entity);
        void Delete(long id);
        void SaveChange();
        Task SaveChangeAsync();
    }
}
