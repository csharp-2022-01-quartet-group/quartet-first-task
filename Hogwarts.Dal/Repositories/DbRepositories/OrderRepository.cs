﻿using Hogwarts.DAL.Entities;
using Hogwarts.DAL.Repositories.Interfaces;

namespace Hogwarts.DAL.Repositories.DbRepositories
{
    public class OrderRepository : BaseRepository<Order>, IOrderRepository
    {
        public OrderRepository(AppDbContext context) : base(context)
        {
        }
    }
}
