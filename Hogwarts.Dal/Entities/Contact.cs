
namespace Hogwarts.DAL.Entities
{
    /// <summary>
    /// ���������� ������
    /// </summary>
    public class Contact : EntityBase
    {
        /// <summary>
        /// ����� ����������� �����
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// ����� ��������
        /// </summary>
        public string Telephone { get; set; }
    }

}
