﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Hogwarts.DAL.Migrations
{
    public partial class OrderModelChanged : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Orders_Courses_CourseID",
                table: "Orders");

            migrationBuilder.DropForeignKey(
                name: "FK_Orders_Users_StudentID",
                table: "Orders");

            migrationBuilder.RenameColumn(
                name: "StudentID",
                table: "Orders",
                newName: "StudentId");

            migrationBuilder.RenameColumn(
                name: "CourseID",
                table: "Orders",
                newName: "CourseId");

            migrationBuilder.RenameIndex(
                name: "IX_Orders_StudentID",
                table: "Orders",
                newName: "IX_Orders_StudentId");

            migrationBuilder.RenameIndex(
                name: "IX_Orders_CourseID",
                table: "Orders",
                newName: "IX_Orders_CourseId");

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_Courses_CourseId",
                table: "Orders",
                column: "CourseId",
                principalTable: "Courses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_Users_StudentId",
                table: "Orders",
                column: "StudentId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Orders_Courses_CourseId",
                table: "Orders");

            migrationBuilder.DropForeignKey(
                name: "FK_Orders_Users_StudentId",
                table: "Orders");

            migrationBuilder.RenameColumn(
                name: "StudentId",
                table: "Orders",
                newName: "StudentID");

            migrationBuilder.RenameColumn(
                name: "CourseId",
                table: "Orders",
                newName: "CourseID");

            migrationBuilder.RenameIndex(
                name: "IX_Orders_StudentId",
                table: "Orders",
                newName: "IX_Orders_StudentID");

            migrationBuilder.RenameIndex(
                name: "IX_Orders_CourseId",
                table: "Orders",
                newName: "IX_Orders_CourseID");

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_Courses_CourseID",
                table: "Orders",
                column: "CourseID",
                principalTable: "Courses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_Users_StudentID",
                table: "Orders",
                column: "StudentID",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
