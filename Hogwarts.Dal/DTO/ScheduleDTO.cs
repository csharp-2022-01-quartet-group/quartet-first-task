﻿using System;

namespace Hogwarts.DAL.DTO
{
    public class ScheduleDTO
    {
        public DateTime LessonDateTime { get; set; }

        public long LessonId { get; set; }
    }
}
