﻿using Hogwarts.DAL.Entities;
using Hogwarts.DAL.Repositories.Interfaces;
using System.Linq;

namespace Hogwarts.DAL.Repositories.DbRepositories
{
    public class CourseRepository : BaseRepository<Course>, ICourseRepository
    {
        public CourseRepository(AppDbContext context) : base(context)
        {
        }

        public Course GetByName(string name)
        {
            return _dbSet.FirstOrDefault(f => f.Name == name);
        }
    }
}
