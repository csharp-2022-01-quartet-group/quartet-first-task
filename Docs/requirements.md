# Функциональные требования к системе онлайн-курсов

1. [Система дистанционного обучения](#system)
2. [Информационная раздел](#info)
3. [Сервис по проведению вебинаров](#service)
4. [Система оценивания](#assesment)
5. [Нефункциональные требования](#non-functional-requirements)
6. [Требования к основным сущностям](#entities)
* [Студент](#student)
* [Преподаватель](#teacher)
* [Администратор](#admin)
* [Описание онлайн-курса](#online-course-desc)
* [Краткая и полная аннотации курса](#course-anotation)
* [Полная аннотации курса](#full-cource-anotation)
* [Структура онлайн-курса](#online-cource-structure)
* [Задача](#task)
* [Анализа успеваемости студентов](#reversibility)

## Система дистанционного обучения <a name="system"></a>
Система дистанционного обучения должна предоставлять возможность курируемого обучения (организации обучения под руководством преподавателей. и
включать следующую функциональность:

1. Для студента  
	a. Регистрация студента  
	b. Покупка курса  
		Реализация покупки курса через корзину (Ввести сущность Order и реализовать сервис-контроллер)
		Возможность отписаться от курса
	с. Предоставления студентам обратной связи  (второстепенно) 
		i. Размещение ДЗ (второстепенно) 
		ii. Просмотр оценок (второстепенно) 
		iii. Чат с преподавателем (второстепенно) 
2. Для преподавателя
	a. Регистрация преподавателя  
	b. Создание/редактирование/закрытие/удаление собственных курсов  (возможно вебинары?)
	c. Отслеживание выполнения заданий студентами, оценка выполнения
	d. Анализа успеваемости студентов  (второстепенно)
3. Модератор  
	a. Блокировка курсов(пометка на удаление?)
	b. Блокировка преподавателей/студентов
4. Администратор  
	a. Возможность блокирования/редактирования студента/преподавателя/модератора
	b. Возможность создание модератора
	b. Возможность удаления/редактирования/блокировка любого курса  
	c. Возможность назначать роли

## Информационная раздел <a name="info"></a> 
1. Должен предоставлять информацию о всех размещенных в системе дистанционного обучения курсах, их целевой аудитории и преподавателях, включая название курсов, их основное содержание, программу, отзывы слушателей, порядок прохождения обучения и получения сертификатов;
2. Раздел доступен всем без исключения: преподавателям, студентам, администраторам и анонимным пользователям;
3. Отзывы в чате могут оставлять только слушатели, зарегистрированные на этот курс;
4. Должен содержать анонсы о наборе студентов на ближайшие онлайн-курсы;
5. Включает онлайн-чат и возможность подписки на информационную рассылку;

## Сервис по проведению вебинаров <a name="service"></a>
Сервис по проведению вебинаров должен включать возможность участия в вебинарах и видеоконференциях без установки дополнительного программного обеспечения 
и функционировать на мобильных устройствах и компьютерах через адресную строку в браузере 
(Интересная тема для развития. Как это вообще делают. Тут интеграция с чем-нибудь, если сил хватит.)

## Система оценивания <a name="assesment"></a>
1. Должен быть определен алгоритм расчета итоговой оценки по курсу.
2. Должны быть определены критерии получения сертификата об успешном освоении курса в виде общего требования к итоговой оценке, а также при необходимости к минимальным оценкам по каждой категории задачи.

# Нефункциональные требования: <a name="non-functional-requirements"></a>

1. Система онлайн-обучения должна быть интернет-приложением.
2. Поддержка языков – только русский.
3. Библиотека ресурсов должна предоставлять возможность сохранения мультимедийных материалов (презентаций, видеороликов..
4. Приложение должно быть microservices-ready.

# Требования к основным сущностям: <a name="entities"></a>

## Студент <a name="student"></a>
1. Login;
2. пароль;
3. фамилия, имя, отчество;

## Преподаватель <a name="teacher"></a>
1. Login;
2. пароль;
3. фамилия, имя, отчество;
4. e-mail
5. место работы;
6. должность;
7. ученая степень;
8. ученое звание.
  
## Администратор <a name="admin"></a>
1. Login;
2. пароль;
3. фамилия, имя, отчество;

## Описание онлайн-курса <a name="online-course-desc"></a>
1. Идентификатор курса;
2. Версия курса;
3. Название курса
4. Общая трудоемкость курса: количество недель обучения, средняя нагрузка в неделю
5. Название  
  a. Полное название  
  b. Сокращенное название  
6. Авторы курса
7. Краткая аннотация курса
8. Полная аннотация курса
9. Информация о выдаваемых сертификатах
  
## Краткая и полная аннотации курса <a name="course-anotation"></a>
В краткой аннотации должно быть отражено основное содержание курса в 1-2 предложениях (до 400 символов).

## Полная аннотации курса <a name="full-cource-anotation"></a>
1. О курсе  
  a. Чему посвящен курс  
  b. Какова цель курса  
2. Какие инновационные технологии обучения используются в курсе
3. Какие результаты обучения будут достигнуты обучающимся после прохождения курса (2-3 строки..
4. Мотивационная фраза.
5. Формат курса  
  a. На сколько недель рассчитан курс?  
  b. Какова недельная учебная нагрузка обучающегося по курсу  
  c. Какова общая трудоемкость курса в неделях/масяцах  
  d. Структура курса и расписание  
  e. Упорядоченный список тем (разделов. курса с кратким описанием (максимум 2 предложения.)  
6. Информационные ресурсы  
  a. При необходимости, список дополнительных источников информации (книги, метод. пособия и т.д.. и ссылки на них.  
7. Часто задаваемые вопросы  
  a. Список часто задаваемых вопросов. Например, необходимо ли специальное программное обеспечение.

## Структура онлайн-курса <a name="online-cource-structure"></a>
Контент курса делится на разделы, подразделы, темы и компоненты (задачи).  
Курс должен быть построен на основе месячного планирования.  
Изучаемый материал каждого месяца должен быть декомпозирован на разделы и подразделы, каждый подраздел должен включать одну или более тем, 
страница может содержать ноль или более компонент.  

 | Раздел      | Подраздел   | Тема        | Компонент   |
 | ----------- | ----------- | ----------- | ----------- |
 | Месяц 1     | Подраздел 1 | Тема 1      | Задача 1    |
 |             | Подраздел 2 | Тема 2      | Задача 2    |
 |             |             | Тема 3      |             |
 | Месяц 2     | Подраздел 3 | Тема 4      | Задача 3    |
 | ...         |             |             |             |

## Задача <a name="task"></a>
Задача - это компонент курса, который предназначен для оценки результатов обучения. Задача на подзадачи не разбивается.

1. Тема
2. Задание
3. Категория (необходима для автоматизации оценки успеваемости.
4. Дополнительные материалы

## Анализа успеваемости студентов <a name="reversibility"></a>
Отчет(ы) об успеваемости.
