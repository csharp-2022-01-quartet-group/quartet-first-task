﻿using Hogwarts.DAL.Entities;
using Hogwarts.DAL.Repositories.Interfaces;
using System.Linq;

namespace Hogwarts.DAL.Repositories.DbRepositories
{
    public class UserRepository : BaseRepository<User>, IUserRepository
    {
        public UserRepository(AppDbContext context) : base(context)
        {
        }

        public User GetByLogin(string login)
        {
            return _dbSet.FirstOrDefault(f => f.Login == login);
        }
    }
}
