﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hogwarts.DAL.Entities
{
    public class EntityBase
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Key]
        public long Id { get; set; }
    }
}
