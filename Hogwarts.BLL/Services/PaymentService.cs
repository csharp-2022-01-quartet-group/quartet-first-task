﻿using Hogwarts.DAL.Entities;
using Hogwarts.DAL.Repositories.Interfaces;
using Hogwarts.BLL.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hogwarts.BLL.Services
{
    public class PaymentService : ServiceBase<IPaymentRepository, Payment>, IPaymentService
    {
        public PaymentService(IPaymentRepository repository) : base(repository)
        {
        }
    }
}
