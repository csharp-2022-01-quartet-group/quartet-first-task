﻿using System.ComponentModel.DataAnnotations;
using System.Data;

namespace Hogwarts.DAL.Entities
{
    /// <summary>
    /// Пользователь
    /// </summary>
    public class User : EntityBase
    {
        /// <summary>
        /// Имя пользователя
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// Фамилия пользователя
        /// </summary>
        /// <returns></returns>
        public string Surname { get; set; }

        /// <summary>
        /// Логин пользователя
        /// </summary>
        /// <returns></returns>
        
        //[indexer{IsUnique=true] - Доступно с EF 6.1, уникальные значения логинов - предложение.
        public string Login { get; set; }

        /// <summary>
        /// Пароль пользователя
        /// </summary>
        /// <returns></returns>
        public string Password { get; set; }

        /// <summary>
        /// Идентификатор сущности роли
        /// </summary>
        /// <returns></returns>
        public long RoleId { get; set; }

        /// <summary>
        /// Сущность роль
        /// </summary>
        /// <returns></returns>
        public virtual Role Role { get; set; }

        /// <summary>
        /// Идентификатор сущности контакты
        /// </summary>
        /// <returns></returns>
        public long? ContactId { get; set; }

        
        /// <summary>
        /// Сущность Контакты
        /// </summary>
        /// <returns></returns>
        public virtual Contact? Contact { get; set; }
        //public override string ToString() => $"{UserId}, {RolId}, {ContactId}";

        /// <summary>
        /// Дата рождения
        /// </summary>
        public string Birthday { get; set; }

        /// <summary>
        /// Пол
        /// </summary>
        public string Sex { get; set; }

    }

}
